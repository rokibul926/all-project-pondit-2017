// Active isotope with jQuery code:
$('.made-content').isotope({
	itemselectord:'.content-box',
	layoutmode:'fitRow'
});
 
 // isotope click function
 $('.iso-nav ul li').click(function(){
 	$('.iso-nav ul li').removeClass('active');
 	$(this).addClass('active');

 		var selector = $(this).attr('data-filter');
 			$('.made-content').isotope({
 				filter: selector
 			});
 			return false;
 });